module Main exposing (..)

import Keyboard exposing (KeyCode)
import Task exposing (Task)
import AnimationFrame
import Model exposing (Model, model)
import View exposing (view)
import Update exposing (update)
import Actions exposing (Action(..))
import Html exposing (Html)


main : Program Never Model Action
main =
    Html.program
        { init = ( model, Task.perform (always Noop) (Task.succeed 0) )
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


key : Bool -> KeyCode -> Action
key on keycode =
    case keycode of
        37 ->
            MoveLeft on

        39 ->
            MoveRight on

        40 ->
            MoveUp on

        38 ->
            MoveDown on

        _ ->
            Noop


subscriptions : Model -> Sub Action
subscriptions model =
    Sub.batch
        [ Keyboard.ups (key False)
        , Keyboard.downs (key True)
        , AnimationFrame.times Tick
        ]
