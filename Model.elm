module Model exposing (Model, model)

import Time exposing (Time)


type alias Model =
    { currentPosition : ( Int, Int )
    , movement : Bool
    , currentTime : Time
    , startTime : Time
    , movementTime : Float
    , distanceMoved : Int
    , prevSpeed : Float
    , timeDelta : Float
    , lastUpdateTime : Time
    }


model : Model
model =
    { currentPosition = ( 0, 0 )
    , movement = False
    , currentTime = 0
    , startTime = 0
    , movementTime = 0
    , distanceMoved = 0
    , prevSpeed = 0
    , timeDelta = 0
    , lastUpdateTime = 0
    }
