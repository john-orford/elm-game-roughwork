module Update exposing (update)

import Model exposing (model, Model)
import Actions exposing (Action(..))
import Time exposing (Time)


-- UPDATE
--distance moved needs to be calced by time diff after each step


update : Action -> Model -> ( Model, Cmd Action )
update action model =
    let
        ( x, y ) =
            model.currentPosition

        startTime =
            if model.movementTime == 0 then
                model.currentTime - 0.001
            else
                model.startTime

        calcMovementTime on =
            if on == True then
                model.currentTime - startTime
            else
                0

        distanceDelta =
            min (1 + (model.distanceMoved // 10)) 20

        distanceMoved on =
            if on then
                distanceDelta + model.distanceMoved
            else
                0

        updateModel on mvmnt =
            if True then
                -- if (Time.inSeconds (model.currentTime - model.lastUpdateTime)) >= 0.03333333333 then
                ( { model
                    | currentPosition = mvmnt
                    , movementTime = calcMovementTime on
                    , startTime = startTime
                    , distanceMoved = distanceMoved on
                    , prevSpeed = (toFloat model.distanceMoved) / model.movementTime
                    , lastUpdateTime = model.currentTime
                  }
                , Cmd.none
                )
            else
                ( model
                , Cmd.none
                )
    in
        case Debug.log "action" action of
            MoveLeft on ->
                updateModel on ( x - distanceDelta, y + 0 )

            MoveRight on ->
                updateModel on ( x + distanceDelta, y + 0 )

            MoveUp on ->
                updateModel on ( x + 0, y - distanceDelta )

            MoveDown on ->
                updateModel on ( x + 0, y + distanceDelta )

            Noop ->
                ( model, Cmd.none )

            Tick time ->
                ( { model
                    | currentTime = time
                    , timeDelta = time - model.currentTime
                  }
                , Cmd.none
                )
