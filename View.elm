module View exposing (view)

import Model exposing (model, Model)
import Actions exposing (Action)
import Html exposing (Html, button, div, text, span)
import Html.Attributes exposing (style)
import Collage
import Element
import Color
import Time exposing (Time)


view : Model -> Html Action
view { currentPosition, movementTime, startTime, distanceMoved, timeDelta, prevSpeed, lastUpdateTime } =
    let
        currentPositionFloat =
            (\( x, y ) -> ( toFloat x, toFloat y )) currentPosition
    in
        div []
            [ span
                []
                [ Element.toHtml
                    (Element.color Color.yellow
                        (Collage.collage
                            1700
                            800
                            [ Collage.move
                                currentPositionFloat
                                (makeSquare Color.red 30)
                            ]
                        )
                    )
                ]
            , div [] [ text ("Strt: " ++ (toString startTime)) ]
            , div [] [ text ("Time in Seconds: " ++ (toString (Time.inSeconds movementTime))) ]
            , div [] [ text ("Distance in Pixels: " ++ (toString distanceMoved)) ]
            , div [] [ text ("Pixels per second: " ++ (toString ((toFloat distanceMoved) / (Time.inSeconds movementTime)))) ]
              -- , div [] [ text ("lUpdT: " ++ (toString lastUpdateTime)) ]
            , div []
                [ text
                    ("Accl: "
                        ++ (toString
                                ((((toFloat distanceMoved) / movementTime)
                                    - prevSpeed
                                 )
                                    / timeDelta
                                )
                           )
                    )
                ]
            ]


makeSquare : Color.Color -> Float -> Collage.Form
makeSquare color size =
    Collage.filled color (Collage.square size)
