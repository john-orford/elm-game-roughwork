module Actions exposing (Action(..))

import Time exposing (Time)


type Action
    = MoveLeft Bool
    | MoveRight Bool
    | MoveUp Bool
    | MoveDown Bool
    | Noop
    | Tick Time
